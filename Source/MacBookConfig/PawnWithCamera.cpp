#include "PawnWithCamera.h"

#include <algorithm>
using std::random_shuffle;
using std::reverse;
#include <cstdint>
using std::int16_t;
using std::uint8_t;
#include <cstdlib>
using std::abs;
#include <utility>
using std::move;

#include "HSV2RGB.h"
using dl::HsvColor;
using dl::HsvToRgb;
using dl::RgbColor;

#include "Kismet/GameplayStatics.h"
#include "MaterialCompiler.h"


// Sets default values
APawnWithCamera::APawnWithCamera()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	SelfieStick = CreateDefaultSubobject<USpringArmComponent>(TEXT("SelfieStick"));
	SelfieStick->SetupAttachment(RootComponent);
	SelfieStick->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 50.0f), FRotator(-60.0f, 0.0f, 0.0f));
	SelfieStick->TargetArmLength = StartArmLength;
	SelfieStick->bEnableCameraLag = true;
	SelfieStick->CameraLagSpeed = 3.0f;

	ZoomFactor = SelfieStick->TargetArmLength / MaxArmLength;

	SelfieCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("SelfieCamera"));
	SelfieCamera->SetupAttachment(SelfieStick, USpringArmComponent::SocketName);
	SelfieCamera->bConstrainAspectRatio = true;
	
	AutoPossessPlayer = EAutoReceiveInput::Player0;
}

// Called when the game starts or when spawned
void APawnWithCamera::BeginPlay()
{
	Super::BeginPlay();

	auto MacBookMeshComponent{ FindComponentByClass<UStaticMeshComponent>() };
	DynamicKeyboardMaterial = UMaterialInstanceDynamic::Create(OriginalKeyboardMaterial, nullptr);
	MacBookMeshComponent->GetStaticMesh()->SetMaterial(7, DynamicKeyboardMaterial);
}

// Called every frame
void APawnWithCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	// zoom
	ZoomFactor = FMath::Clamp(ZoomFactor, 0.0f, 1.0f);
	SelfieStick->TargetArmLength = FMath::Lerp(MinArmLength, MaxArmLength, ZoomFactor);
	// pitch & yaw
	{
		FRotator NewRotation = SelfieStick->GetComponentRotation();
		NewRotation.Pitch = FMath::Clamp(NewRotation.Pitch + CameraInput.Y, -89.0f, 89.0f);
		NewRotation.Yaw += CameraInput.X;
		SelfieStick->SetWorldRotation(NewRotation);
	}
	// camera change timer
	CameraChangeDuration -= DeltaTime;
}

// Called to bind functionality to input
void APawnWithCamera::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// actions
	InputComponent->BindAction("ZoomIn", IE_Pressed, this, &APawnWithCamera::ZoomIn);
	InputComponent->BindAction("ZoomOut", IE_Pressed, this, &APawnWithCamera::ZoomOut);
	InputComponent->BindAction("ChangeColor", IE_Released, this, &APawnWithCamera::ChangeKeyboardColor);
	InputComponent->BindAction("ChangeView", IE_Released, this, &APawnWithCamera::ChangeView);
	InputComponent->BindAction("MoveCamera", IE_Pressed, this, &APawnWithCamera::EnableCameraMovement);
	InputComponent->BindAction("MoveCamera", IE_Released, this, &APawnWithCamera::DisableCameraMovement);
	// axis
	InputComponent->BindAxis("CameraPitch", this, &APawnWithCamera::PitchCamera);
	InputComponent->BindAxis("CameraYaw", this, &APawnWithCamera::YawCamera);
}

void APawnWithCamera::ZoomIn()
{
	ZoomFactor -= 0.05f;
}

void APawnWithCamera::ZoomOut()
{
	ZoomFactor += 0.05f;
}

void APawnWithCamera::ChangeKeyboardColor()
{
	if (RandomColors.empty())
	{
		const auto AreColorsSimiliar{
			[] (const HsvColor& rhv, const HsvColor& lhv) -> bool
			{
				// As I am casting uint8_t to a signed interger value, I will use more bits to make sure that they fit.
				const int16_t ColorSimiliarityAlpha{ 15 };
				return abs(static_cast<int16_t>(rhv.H) - static_cast<int16_t>(lhv.H)) < ColorSimiliarityAlpha;
			}
		};

		uint8 ColorSpread{ static_cast<uint8>(255 / ColorVariance) };
		for (uint8 i{ 0u }; i < ColorVariance; ++i)
		{
			const float Variance{ FMath::RandRange(-25.0f, 25.0f) };
			float Hue{ ColorSpread * i + Variance };
			if (Hue > 255.0f)
				Hue -= 255.0f;
			HsvColor color{ static_cast<uint8_t>(Hue), 255u, static_cast<uint8_t>(FMath::RandRange(127.0f, 255.0f)) };
			// This won't cause empty vector in the end as I am drawing colors from the full hue range.
			if (AreColorsSimiliar(LastColor, color))
				continue;
			RandomColors.emplace_back(move(color));
		}
		random_shuffle(RandomColors.begin(), RandomColors.end());
		LastColor = RandomColors.back();
	}
	RgbColor RgbColor{ HsvToRgb(move(RandomColors.back())) };
	RandomColors.pop_back();
	DynamicKeyboardMaterial->SetVectorParameterValue(TEXT("Param"), FColor(RgbColor.R, RgbColor.G, RgbColor.B));
}

void APawnWithCamera::ChangeView()
{
	if (CameraChangeDuration > 0.0f)
		return;

	APlayerController* PlayerController{ UGameplayStatics::GetPlayerController(this, 0) };
	if (!PlayerController)
		return;

	if (!SelfieCameraActor)
		SelfieCameraActor = PlayerController->GetViewTarget();

	bFreeCameraView = !bFreeCameraView;
	PlayerController->SetViewTargetWithBlend(
		bFreeCameraView ? SelfieCameraActor : StaticCamera,
		CameraSwitchTimeInSeconds
	);
	CameraChangeDuration = CameraSwitchTimeInSeconds;
}

void APawnWithCamera::EnableCameraMovement()
{
	bCameraMovement = true;
}

void APawnWithCamera::DisableCameraMovement()
{
	bCameraMovement = false;
	CameraInput.X = 0.0f;
	CameraInput.Y = 0.0f;
}

void APawnWithCamera::PitchCamera(float AxisValue)
{
	if (bCameraMovement && bFreeCameraView)
	{
		CameraInput.Y = AxisValue;
	}
}

void APawnWithCamera::YawCamera(float AxisValue)
{
	if (bCameraMovement && bFreeCameraView)
	{
		CameraInput.X = AxisValue;
	}
}
