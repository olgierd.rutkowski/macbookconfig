// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <vector>

#include "HSV2RGB.h"

#include "Camera/CameraComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/SpringArmComponent.h"
#include "PawnWithCamera.generated.h"


UCLASS()
class MACBOOKCONFIG_API APawnWithCamera : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APawnWithCamera();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere)
	float MaxArmLength = 400.0f;

	UPROPERTY(EditAnywhere)
	float MinArmLength = 0.0f;

	UPROPERTY(EditAnywhere)
	float StartArmLength = 200.0f;

	UPROPERTY(EditAnywhere)
	float CameraSwitchTimeInSeconds = 1.0f;
	
	UPROPERTY(EditAnywhere)
	AActor* StaticCamera = nullptr;

	UPROPERTY(EditAnywhere)
	float ColorRangeBase = 10.0f;

	UPROPERTY(EditAnywhere)
	float ColorRangeSpread = 80.0f;

	UPROPERTY(EditAnywhere)
	uint8 ColorVariance = 9u;
	
	UPROPERTY(EditAnywhere)
	UMaterial* OriginalKeyboardMaterial;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	void ZoomIn();
	void ZoomOut();
	void ChangeKeyboardColor();
	void ChangeView();
	void EnableCameraMovement();
	void DisableCameraMovement();
	void PitchCamera(float AxisValue);
	void YawCamera(float AxisValue);

	
	USpringArmComponent* SelfieStick;
	UCameraComponent* SelfieCamera;

	AActor* SelfieCameraActor = nullptr;

	//UMaterialInterface* OriginalKeyboardMaterial = nullptr;
	UMaterialInstanceDynamic* DynamicKeyboardMaterial = nullptr;

	FVector2D CameraInput;
	float ZoomFactor = 0.0f;

	bool bCameraMovement = false;
	bool bFreeCameraView = true;
	float CameraChangeDuration = 0.0f;

	float PreviousHue = 0u;
	std::vector<dl::HsvColor> RandomColors;
	dl::HsvColor LastColor{ 0u, 0u, 0u };
};
