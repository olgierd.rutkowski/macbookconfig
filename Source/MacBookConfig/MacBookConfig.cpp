// Copyright Epic Games, Inc. All Rights Reserved.

#include "MacBookConfig.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MacBookConfig, "MacBookConfig" );
