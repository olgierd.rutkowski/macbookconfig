#pragma once

#include <cstdint>

namespace dl
{
    struct RgbColor
    {
        std::uint8_t R;
        std::uint8_t G;
        std::uint8_t B;
    };

    struct HsvColor
    {
        std::uint8_t H;
        std::uint8_t S;
        std::uint8_t V;
    };

    RgbColor HsvToRgb(const HsvColor& Hsv);
}
