#include "HSV2RGB.h"
using dl::HsvColor;
using dl::RgbColor;
using std::uint8_t;

#include <cmath>
using std::fabs;
using std::fmod;


RgbColor dl::HsvToRgb(const HsvColor& Hsv)
{
    RgbColor rgb{};
    uint8_t region, remainder, p, q, t;

    if (Hsv.S == 0)
    {
        rgb.R = Hsv.V;
        rgb.G = Hsv.V;
        rgb.B = Hsv.V;
        return rgb;
    }

    region = Hsv.H / 43;
    remainder = (Hsv.H - (region * 43)) * 6;

    p = (Hsv.V * (255 - Hsv.S)) >> 8;
    q = (Hsv.V * (255 - ((Hsv.S * remainder) >> 8))) >> 8;
    t = (Hsv.V * (255 - ((Hsv.S * (255 - remainder)) >> 8))) >> 8;

    switch (region)
    {
    case 0:
        rgb.R = Hsv.V; rgb.G = t; rgb.B = p;
        break;
    case 1:
        rgb.R = q; rgb.G = Hsv.V; rgb.B = p;
        break;
    case 2:
        rgb.R = p; rgb.G = Hsv.V; rgb.B = t;
        break;
    case 3:
        rgb.R = p; rgb.G = q; rgb.B = Hsv.V;
        break;
    case 4:
        rgb.R = t; rgb.G = p; rgb.B = Hsv.V;
        break;
    default:
        rgb.R = Hsv.V; rgb.G = p; rgb.B = q;
        break;
    }

    return rgb;
}
