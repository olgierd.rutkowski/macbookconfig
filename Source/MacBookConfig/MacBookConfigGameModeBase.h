// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MacBookConfigGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MACBOOKCONFIG_API AMacBookConfigGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
